const getProducts = (state, action) => {
  return Object.assign({}, state, {
    products: action.response
  });
}

export default getProducts;