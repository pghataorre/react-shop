const deleteProducts = (state, action, calculateTotal) => {
  const deleteProdId = action.prodId;
  const basketContent = state.basket;
  const deleteBasketItem = basketContent.basketItems;
  let prodIndex;
  let i;

  for (i=0; i < deleteBasketItem.length; i++) {
    if (deleteBasketItem[i].id === deleteProdId) {
      prodIndex = i;
      break;
    }
  }

  deleteBasketItem.splice(prodIndex, 1);

  return Object.assign({}, state, {
    basket: {
      basketItems: deleteBasketItem,
      basketCount: state.basket.basketCount-=1,
      totals: calculateTotal(basketContent.basketItems)
    }
  });
}

export default deleteProducts;