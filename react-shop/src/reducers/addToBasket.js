const addToBasket = (state, action, filterCurrentBasket, calculateTotal) => {
  let basket = state.basket;
  const addBasket = basket.basketItems;
  const prodItemId = action.prodId;
  const prodItem = state.products[prodItemId];
  const quantity = filterCurrentBasket(state, prodItemId);

  if (quantity === 0) {
    prodItem.prodQty = 1;
    addBasket.push(prodItem);
  } else {
    for (let i=0; i < addBasket.length; i++) {

      if (addBasket[i].id === prodItemId) {
        let qty = parseInt(addBasket[i].prodQty);
        qty+=1;
        addBasket[i].prodQty = qty;
        break;
      }
    }
  }

  const totals = calculateTotal(addBasket);

  return Object.assign({}, state, {
    basket: {
      basketItems: addBasket,
      basketCount: basket.basketCount+=1,
      totals
    }
  });
}

export default addToBasket;