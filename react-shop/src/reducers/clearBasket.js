const clearBasket = (state) => {
  return Object.assign({}, state, {
    basket: {
      basketItems: [],
      basketCount: 0,
      totals: 0.00
    }
  });
}

export default clearBasket;