const changeProductQty = (state, action, calculateTotal) => {
  const prodId = action.prodId;
  const qtyVal = action.changeVal;
  const basketQtyChange = state.basket.basketItems;


  for (let i=0; i < basketQtyChange.length; i++) {
    if (basketQtyChange[i].id === prodId) {
      basketQtyChange[i].prodQty = qtyVal;
      break;
    }
  }

  const totals = calculateTotal(basketQtyChange);

  return Object.assign({}, state, {
    basket: {
      basketItems: basketQtyChange,
      basketCount: state.basket.basketCount,
      totals
    }
  });
}

export default changeProductQty;