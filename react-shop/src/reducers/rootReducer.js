import initState from '../initialState/initialState.js';
import deleteProduct from './deleteProduct.js';
import clearBasket from './clearBasket.js';
import changeProductQty from './changeProductQty.js';
import addToBasket from './addToBasket.js';
import getProducts from './getProducts.js';

const rootReducer  = (state = initState, action) => {
  switch(action.type) {
    case 'GET_DATA':
      return getProducts(state, action);

    case 'ADD_TO_BASKET':
      return addToBasket(state, action, filterCurrentBasket, calculateTotal);

    case 'CLEAR_BASKET':
      return clearBasket(state);
    
    case 'DELETE_PRODUCT':
      return deleteProduct(state, action, calculateTotal);

    case 'CHANGE_PROD_QTY':
      return changeProductQty(state, action, calculateTotal);

    default: 
      return state;
  }
}

const filterCurrentBasket = (state, prodItemId) => {
  return state.basket.basketItems.filter((item) => {
    return item.id === prodItemId;
  }).length
}

const calculateTotal = (basketItems) => {
  if (basketItems.length > 0) {
    let i;
    let prodSubTotal = 0;
    let total = null;

    for (i=0; i < basketItems.length; i++) {
      prodSubTotal = (basketItems[i].price.toFixed(2) * basketItems[i].prodQty);
      total = total + prodSubTotal;
    }

    return total.toFixed(2);
  }

  return 0.00;
}

export default rootReducer;