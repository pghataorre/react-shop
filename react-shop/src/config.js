const config = {
  apiUrl: 'http://localhost:3333/',
  path: 'get-products',
  maxAttempts: 3,
  defaultCurrencySymbol: '£'
}

export default config;