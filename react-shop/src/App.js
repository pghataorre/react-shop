import React from 'react';
import { connect } from 'react-redux';
import Header from './components/header.js';
import ProductList from './components/productList.js';
import ProdBasket from './components/prodBasket.js';
import './styles/main.scss';

function App(props) {
  return (
    <div className="App">
      <main>
        <ProdBasket basketContent={props.basketContent} />
        <Header />
        <ProductList />
      </main>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    basketContent: state.basket
  }
}

export default connect(mapStateToProps)(App);
