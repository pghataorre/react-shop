import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProductDetail from './productDetail.js';
import config from '../config.js';
class ProductList extends  Component {
  state = {
    showLoadError: false,
    attempts: 1
  }

  addToBasket = (prodId) => {
    this.props.addToBasket(prodId);
  }

  getData = () => {
    const url = `${config.apiUrl}${config.path}`

    return new Promise((resolve, reject) => {
      fetch(url)
        .then((response) => {
          return response.json();
        })
        .then((response) => {
          this.props.getData(response)
          resolve({sucess: true})
        })
        .catch((err) => {
          console.log('ERROR', err)
          reject({sucess: false})
        });
    })
  }

  componentDidMount() {
    let count = 0;

    const time = setInterval( () => {
      this.getData()
        .then(() => {
          clearInterval(time);
        })
        .catch(() => {
          if (count >= config.maxAttempts) {
            clearInterval(time);
            this.setState({showLoadError: true})
          }
          this.setState({attempts: count+=1});
        });
    }, 1000);
  }
  
  render() {
    const products = this.props.products;

    return (
      <section>
        <h2>Product listing</h2>
        { 
        products.length > 0 ? 
        products.map((item) => {
          return (
            <div className="list-container" key={item.id}>
              <ProductDetail addToBasket={this.addToBasket} products={item}/>
            </div>
            )
          })
        : (<h3>GETTING PRODUCTS ATTEMPT ... {this.state.attempts}</h3>)}
      { this.state.showLoadError ? (<h3>PLEASE TRY AGAIN SOON WE CAN'T PRODUCTS RIGHT NOW</h3>) : null }
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.products,
    basket: state.basket
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToBasket: (prodId) => {
      dispatch({type: 'ADD_TO_BASKET', prodId});
    },
    getData: (response) => {
      dispatch({type: 'GET_DATA', response});
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
