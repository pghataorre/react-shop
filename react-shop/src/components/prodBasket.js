import React from 'react';
import { connect } from 'react-redux';
import ProdBasketTotals from './prodBasketTotals.js';
import config from '../config.js';

const ProdBasket = (props) => {
  const basketItems = props.basketContent.basketItems;
  const basketCount = props.basketContent.basketCount;

  const calculateProdSubTotals = (price, prodQty) => {
    return `${config.defaultCurrencySymbol} ${(price * parseInt(prodQty)).toFixed(2)}`;
  }

  const changeContent = (changeVal, prodId) => {
    const regFilter = new RegExp('^[0-9]+$');

    if (regFilter.test(changeVal)) {
      props.changeContent(changeVal, prodId)
    }
  }

  const deleteProduct = (prodId) => {
    props.deleteProduct(prodId);
  }

  return (
    <section className='basket-container'>
      { basketCount > 0 ? (
      <ol className="basket-detail">
          {basketItems.map(item => {
            const {id, name, price, prodQty} = item;

            return (
              <li key={id}>
                <p>
                  <span className="basket-detail__name">{name}</span>
                  <span className="basket-detail__qty">
                    <label htmlFor="prod-quantity-detail" className="indent-text basket-detail__name">{name}</label>
                    <input type="text" onChange={ (event)=> {changeContent(event.target.value, id)} } id={`prod-quantity-detail${id}`} value={prodQty} />
                  </span>
                  <span className="basket-detail__price">
                    {calculateProdSubTotals(price, prodQty)}
                  </span>
                  <span className="basket-detail__delete-product">
                    <button id="delete-product" onClick={ () => deleteProduct(id) }>
                      <span className="indent-text">Delete Item</span>
                    </button>
                  </span>
                </p>
              </li>
          )
        })}
        {basketCount > 0 ? (<ProdBasketTotals />): null}
      </ol>) : null}
  </section>
  )
}

const mapStateToProps = (state) => {
  return {
    basketContent: state.basket
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteProduct: (prodId) => {
      dispatch({type: 'DELETE_PRODUCT', prodId });
    },
    changeContent: (changeVal, prodId) => {
      dispatch({type: 'CHANGE_PROD_QTY', changeVal, prodId});
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProdBasket);
