import React from 'react';
import { connect } from 'react-redux';
import config from '../config.js';
  
  const ProdBasketTotals = (props) => {
    const basktTotal = `${config.defaultCurrencySymbol} ${props.basktTotal}`;


    const clearAll = () => {
      props.clearAll();
    }

    return (
      <li className="basket-summary">
        <span className="basket-summary__total">
          {basktTotal}
        </span>
        <span className="basket-summary__clear">
          <button id="clear_product" onClick={clearAll}>Clear</button>
        </span>
        <span className="basket-summary__check-out">
          <button id="checkout" onClick={() => alert('TO CHECKOUT')}>
            Check Out
            <span className="chevron-icon"></span>
          </button>
        </span>
    </li>
    )
  }

  const mapStateToProps = (state) => {
    return {
      basketItems: state.basket.basketItems,
      basktTotal: state.basket.totals
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      clearAll: () => {
        dispatch({type: 'CLEAR_BASKET'});
      },
    };
  }

  export default connect(mapStateToProps, mapDispatchToProps)(ProdBasketTotals)