import React from 'react';

const BasketLink = (props) => {
  const basketCount = props.basketCount;

  return (
    <button className="basket-link" id="show-basket">
      <span className="basket-link__basket-content">{basketCount}</span>
      <span className="indent-text">{basketCount} Item(s) in Basket</span>
    </button>
  );
}

export default BasketLink;
