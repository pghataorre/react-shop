import React from 'react';
import config from '../config.js';

const ProductDetail = (props) => {
  const {id, name, description, price, image} = props.products;
  const formattedPrice = `${config.defaultCurrencySymbol} ${price.toFixed(2)}`;

  return (
    <ul className="product-detail">
      <li>
        <p className="product-detail__info-title">Product:</p>
        <p className="product-detail__detail">{name}</p>
        <p className="product-detail__info-title">Descripton: </p>
        <p className="product-detail__detail">{description}</p>
        <p className="product-detail__info-title">Price</p>
        <p className="product-detail__price">{formattedPrice}</p>
        <div className="product-detail__button_container">
        <button onClick={ () => props.addToBasket(id) }>Add to basket</button>
        </div>
      </li>
      <li className="product-detail__image-list-container">
        <p className="product-detail__info-title">Image:</p>
        <div className="product-detail__image">
          <img src={image} alt={name} />
        </div>
      </li>
    </ul>
  )
}

export default ProductDetail;
