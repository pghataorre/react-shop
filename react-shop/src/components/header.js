import React from 'react';
import { connect } from 'react-redux';
import BasketLink from './basketLink.js'

const Header = (props) => {
  const basketCount = props.basketCount;
  
  return (
    <header>
      <h1>Shopping Test</h1>
      {basketCount > 0 ? (<BasketLink basketCount={basketCount} />) : null}
    </header>
  );
}

const mapStateToProps = (state) => {
  return {
    basketCount: state.basket.basketCount
  }
}

export default connect(mapStateToProps)(Header)