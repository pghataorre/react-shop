const initState = {
  products: [],
  basket: {
    basketItems: [],
    basketCount: 0,
    totals: 0.00
  }
};

export default initState;


